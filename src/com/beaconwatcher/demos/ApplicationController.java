/**  
* ApplicationController.java-Main application controller class
* @author  Shai Baz
* @version 1.0 
* @see Application
*/
package com.beaconwatcher.demos;

import android.app.Application;
import android.app.ProgressDialog;
import android.content.Context;
import android.view.Gravity;
import android.widget.Toast;

import com.radiusnetworks.ibeacon.IBeacon;

/**
 * Main application controller class to keep reference for
 * global variables across multiple Activities.
 */
public class ApplicationController extends Application {
	private ProgressDialog mPd;
	private IBeacon mSelBeacon=null;
	
	
	/**
	 * Keeps reference of selected beacon
	 * to be used by a different activity
	 * 
	 * @param beacon A variable of type IBeacon.
	 */
	public void setSelectedBeacon(IBeacon beacon){
		mSelBeacon=beacon;
	}
	
	/**
	 * Get reference of selected beacon
	 * for using into another activity.
	 */
	public IBeacon getSelectedBeacon(){
		return mSelBeacon;
	}
	
	
	/**
	 * Shows ProgressDialog over any activity
	 * @param c This is the context of activity in which progress dialog needs to displayed.
	 * @param title This is the String to be displayed as title of dialog. 
	 */
	public void showDialog(Context c, String title, String msg){
		mPd=ProgressDialog.show(c, title, msg);
	}
	
	/**
	 * Removes the ProgressDialog
	 */
	public void hideDialog(){
		if (mPd!=null){
			mPd.dismiss();
			mPd=null;
		}
	}
	
	/**
	 * Shows a Toast over any activity
	 * 
	 * @param c This is the context of activity in which Toast needs to displayed.
	 * @param message String variable to be displaed as message of activity.
	 */
	public static void showToast(Context context, String message) {
		try {
			if (context != null) {
				int duration = Toast.LENGTH_SHORT;
				Toast toast = Toast.makeText(context, message, duration);
				toast.show();
				toast.setGravity(Gravity.CENTER_VERTICAL|Gravity.CENTER_HORIZONTAL, 0, 0);
			}
		} catch (Exception exception) {
			exception.printStackTrace();
		}
	}
}
