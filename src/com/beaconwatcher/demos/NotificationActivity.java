/**  
 * DistanceActivity.java -Displays a Status Bar Notification
 * @author  Shai Baz
 * @version 1.0 
 * @see RangeNotifier
 * @see IBeaconConsumer
 * @see IBeaconDataNotifier
 */

package com.beaconwatcher.demos;

import java.util.Collection;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.RemoteException;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.radiusnetworks.ibeacon.IBeacon;
import com.radiusnetworks.ibeacon.IBeaconConsumer;
import com.radiusnetworks.ibeacon.IBeaconManager;
import com.radiusnetworks.ibeacon.MonitorNotifier;
import com.radiusnetworks.ibeacon.RangeNotifier;
import com.radiusnetworks.ibeacon.Region;

/**
 * Shows notifications upon reaching at certain distance towards
 * the beacon.
 * 
 * It demonstrates about how real notifications will work
 * in an real apps.
 */
public class NotificationActivity extends Activity implements IBeaconConsumer  {
	protected static final String TAG = "NotificationActivity";
	private Context mContext;
	private ApplicationController mApp;
	private ProgressDialog mPd;

	private NotificationManager notificationManager;
	private static final int NOTIFICATION_ID = 123;


	/**
	 * check if ranged beacon has got some notification data.
	 */
	protected Boolean gotNotificationData = false;


	/**
	 * Keep reference of all notification set in API for this beacon.
	 */
	protected JSONArray notifications;


	/**
	 * Keep track of last zone entered
	 */
	protected Integer lastEnteredZone = 0;


	/**
	 * This is a Region object for monitoring/ranging all beacons.
	 */
	private Region mRegion;

	/**
	 * You must create this instance variable to use
	 * SDK's main manager class IBeaconManager.
	 * It takes current activity and returns the instance.
	 * Use this instance to bind/unbind your activity
	 * as well as for starting/stopping Ranging and Monitoring. 
	 */
	private IBeaconManager iBeaconManager = IBeaconManager.getInstanceForApplication(this);


	private LinearLayout mContainer;
	private TextView mDiscount;
	private TextView mMessage;
	private TextView mStatus;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		Log.d(TAG, "oncreate");
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_notification);

		mContainer = (LinearLayout) this.findViewById(R.id.note_cont);
		mStatus = (TextView) this.findViewById(R.id.notification_status);

		mContext=NotificationActivity.this;
		mApp = (ApplicationController) getApplicationContext();
		
		//For creating Status Bar Notifications.
		notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
		
		
		//Define Region by using uuid, major and minor values
		//from selected beacon by previous activity.
		//App will monitor this region for enter/exit events.
		mRegion = new Region("TestRegion", mApp.getSelectedBeacon().getProximityUuid(), mApp.getSelectedBeacon().getMajor(), mApp.getSelectedBeacon().getMinor());

		//On Startup, we call online api to fetch all notifications
		//for this given beacon (created using online manager).
		//If this beacons is not already added in the database
		//It will be added and get "DEFAULT" notification data.
		getNotificationFromAPI(mApp.getSelectedBeacon());
	}

	@Override 
	protected void onPause() {
		super.onPause();
		
		if (iBeaconManager.isBound(this)){
			
			// This method notifies the iBeacon service that the IBeaconConsumer is either moving to background mode or foreground mode
			// When in background mode, BluetoothLE scans to look for iBeacons are executed less frequently in order to save battery life
			// The specific scan rates for background and foreground operation are set by the defaults below, but may be customized.
			// Note that when multiple IBeaconConsumers exist, all must be in background mode for the the background scan periods to be used
			// When ranging in the background, the time between updates will be much less fequent than in the foreground.  Updates will come
			// every time interval equal to the sum total of the BackgroundScanPeriod and the BackgroundBetweenScanPeriod
			// All IBeaconConsumers are by default treated as being in foreground mode unless this method is explicitly called indicating
			// otherwise.
			// 
			// Default scan value for foreground mode 1100 miliseconds and background mode is 5500 miliseconds.
		     
			iBeaconManager.setBackgroundMode(this,  true);
		}
	}
	@Override 
	protected void onResume() {
		super.onResume();
		if (iBeaconManager.isBound(this)){
			//Activity is brought into foreground.
			//Set monitoring/ranging into foreground mode.
			iBeaconManager.setBackgroundMode(this, false);    		
		}
	}

	@Override 
	protected void onDestroy() {
		super.onDestroy();
		if (iBeaconManager.isBound(this)){
			iBeaconManager.unBind(this);
		}
	}


	private void showInstruction(final String str) {
		runOnUiThread(new Runnable() {
			public void run() {
				mStatus = (TextView) NotificationActivity.this.findViewById(R.id.notification_status);
				mStatus.setText(str);
			}
		});
	}
	
	
	
	/**
	 * Called when the iBeacon service is running and ready to accept your commands through the IBeaconManager
	 */
	
	@Override
	public void onIBeaconServiceConnect() {

		//Create and set a MonitorNotifier.
		iBeaconManager.setMonitorNotifier(new MonitorNotifier() {

			//This method is Invoked when any beacon found
			//which matches UUID, Major and Minor set to specified Region.
			@Override
			public void didEnterRegion(Region region) {
				//We assume we are inside the vicinity of specified Region
				//Now start Ranging feature to measure distance between
				//Zones of beacons.
				startRanging();
				showInstruction("Entered in the range of beacon. Keep walking towards beacon to see more notifications.");
			}

			
			//On exit of Region, we should stop Ranging to save Battery consumption.
			//Ranging will be started again by enter of Region.
			@Override
			public void didExitRegion(Region region) {
				postNotification("BeaconWatcher", "Goodbye, you will find out more discounts on your next arrival.", true);
				showInstruction("Exited region. Now come back. Pay attention to notification bar.");
				hideContainer();
				stopRanging();

				lastEnteredZone = 0;
			}

			@Override
			public void didDetermineStateForRegion(int state, Region region) {
				//logToDisplay("I have just switched from seeing/not seeing iBeacons: "+state);     
			}
		});

		try {

			//This will invoke IBeaconManger to start Monitoring region.
			iBeaconManager.startMonitoringBeaconsInRegion(mRegion);
		}
		catch (RemoteException e){

		}
	}
	
	
	
	
	/**
	 * This will start Ranging feature, it will scan all BLE devices
	 * every 1100  millisecond and set the results via
	 * callback method "didRangeBeaconsInRegio
 	 */
	
	protected void startRanging(){
		iBeaconManager.setRangeNotifier(new RangeNotifier() {
			
			//Found all beacons inside the Region
			@Override 
			public void didRangeBeaconsInRegion(Collection<IBeacon> iBeacons, Region region) {
				if (iBeacons.size() > 0) {
					
					//Get first beacon in the region and check
					//If any notification content is already loaded.
					IBeacon beacon = iBeacons.iterator().next();
					processNotification(beacon);
				}
			}
		});

		try {
			iBeaconManager.startRangingBeaconsInRegion(mRegion);
		}
		catch (RemoteException e) {   }

	}


	protected void stopRanging(){
		try {
			iBeaconManager.stopRangingBeaconsInRegion(mRegion);
		}
		catch (RemoteException e) {   }
	}




	private void hideContainer() {
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				LinearLayout cont = (LinearLayout) NotificationActivity.this.findViewById(R.id.note_cont);
				cont.setVisibility(View.GONE);
			}
		});
	}


	private void showContainer() {
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				LinearLayout cont = (LinearLayout) NotificationActivity.this.findViewById(R.id.note_cont);
				cont.setVisibility(View.VISIBLE);
			}
		});
	}


	/*
	 * Get a notification for given beacon
	 * We create notification in BeaconWatcher Control Panel.
	 */
	 public boolean getNotificationFromAPI(IBeacon beacon) {
		 AsyncHttpClient client = new AsyncHttpClient();
		 RequestParams params = new RequestParams();

		 //Key for Demo APP = M19fV2F0Y2hJdA==
		 params.put("key", "M19fV2F0Y2hJdA==");
		 params.put("msb_uuid",  beacon.getProximityUuid().toLowerCase().replaceAll("[\\-\\s]", ""));
		 params.put("msb_major", Integer.toString(beacon.getMajor()));
		 params.put("msb_minor", Integer.toString(beacon.getMinor()));

		 try {
			 client.get("http://api.beaconwatcher.com?action=getNotifications", params, serverResponse);
		 } catch (Exception e) {
			 // TODO: handle exception
			 e.printStackTrace();
		 }
		 return true;
	 }


	 public AsyncHttpResponseHandler serverResponse = new AsyncHttpResponseHandler(){
		 @Override
		 public void onStart() {
			 mPd = new ProgressDialog(mContext);
			 mPd.setTitle("Server Call");
			 mPd.setMessage("Connection to API...");
			 mPd.show();
		 };

		 @Override
		 public void onSuccess(String response) {
			 mPd.dismiss();

			 //showInstruction(response);

			 JSONObject jo=null;
			 try{
				 jo=new JSONObject(response);
				 int status=jo.getInt("status");
				 if(status==0){
					 mApp.showToast(mContext,  jo.getString("message"));
				 }
				 else{
					 //Notification found
					 //{"status":1,"message":"Notification","data":[{"nt_id":"2","nt_name":"Another Test","nt_title":"Test This","nt_details":"Test this notification","zone":"In Middle","template":null}]}

					 //All the notifications for this beacon.
					 notifications = jo.getJSONArray("data");
					 gotNotificationData = true;

					 //Start Ranging etc.
					 iBeaconManager.bind(NotificationActivity.this);
				 }
			 }
			 catch(JSONException e){
				 Log.e("log_tag", "Error parsing data "+e.toString());
			 }
		 };

		 @Override
		 public void onFailure(Throwable error, String content) {
			 mPd.dismiss();
			 Log.e("On Failure", "onFailure error : " + error.toString()
					 + "content : " + content);
		 }

		 @Override
		 public void onFinish() {
			 Log.v("On Finish", "onFinish");
		 }
	 };


	 /**
	  * Checks if given beacon has a notification conent
	  * If it has content to display, post a status bar notification
	  * clicking that status bar notification will invoke this activity
	  * and display the contnet.
	  */
	 private void processNotification(IBeacon beacon){
		 // There are three zones defined in IBeacon class.
		 // 1-IMMEDIATE (Less than half a meter away)
		 // 2-NEAR (More than half a meter away, but less than four meters away)
		 // 3-FAR (More than four meters away).
		 
		 // We need to know which zone of the beacon we have enetered
		 Integer zone = beacon.getProximity();

		 //On each loop if we are in same zone then return.
		 if(lastEnteredZone==zone){
			 return;
		 }

		 //If current Zone is 'Near' and last zone was "Immediate"
		 //No notification required in this demo.
		 if(zone==2 && lastEnteredZone==1){
			 lastEnteredZone = 2;
			 showNotificationText("Nearby Zone Exited", " ");
			 return;
		 }


		 //If current Zone is 'Far' and last zone was "Near"
		 //No notification required in this demo.
		 if(zone==3 && lastEnteredZone==2){
			 lastEnteredZone = 3;
			 showNotificationText("Middle Zone Exited", " ");
			 return;
		 }

		 //Assume we are in Immediage Zone, retrieve proper notification from the data
		 //and trigger that notification for the zone.
		 for(Integer i=0; i<notifications.length(); i++){
			 try{
				 JSONObject notification=notifications.getJSONObject(i);
				 //notification data format
				 //{"nt_id":"2","nt_name":"Another Test","nt_title":"Test This","nt_details":"Test this notification","zone":"In Middle","template":null}
				 String noteZone = notification.getString("zone");

				 //If notification zone matches the zone of beacon we entered.
				 if(noteZone.equals(zone.toString())){
					 showContainer();
					 String title = notification.getString("nt_title");
					 String txt = notification.getString("nt_details");
					 postNotification(title, txt, true);
					 
					 //Change last entered zone reference to
					 //solve the repeatitive notifications.
					 lastEnteredZone=zone;
				 }
			 }
			 catch(JSONException e){
				 Log.e("log_tag", "Error parsing data "+e.toString());
			 }
		 }
	 }

	 
	 /**
	  * Post a Status Bar Notification.
	  */
	 private void postNotification(final String title, final String msg, final Boolean showMsg) {
		 Intent notifyIntent = new Intent(mContext, NotificationActivity.class);
		 notifyIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
		 PendingIntent pendingIntent = PendingIntent.getActivities(
				 mContext,
				 0,
				 new Intent[]{notifyIntent},
				 PendingIntent.FLAG_UPDATE_CURRENT);
		 Notification notification = new Notification.Builder(mContext)
		 .setSmallIcon(R.drawable.beacon_gray)
		 .setContentTitle("BeaconWatcher Demo App")
		 .setContentText(msg)
		 .setAutoCancel(true)
		 .setContentIntent(pendingIntent)
		 .build();
		 notification.defaults |= Notification.DEFAULT_SOUND;
		 notification.defaults |= Notification.DEFAULT_LIGHTS;
		 notificationManager.notify(NOTIFICATION_ID, notification);

		 if(showMsg==true){
			 showNotificationText(title, msg);
		 }
	 }

	 private void showNotificationText(final String title, final String msg){
		 // Note that results are not delivered on UI thread.
		 runOnUiThread(new Runnable() {
			 @Override
			 public void run() {
				 mContainer.setVisibility(View.VISIBLE);

				 TextView d = (TextView) NotificationActivity.this.findViewById(R.id.notification_title);
				 d.setText(title);

				 TextView t = (TextView) NotificationActivity.this.findViewById(R.id.notification_msg);
				 t.setText(msg);
			 }
		 });
	 }

}
