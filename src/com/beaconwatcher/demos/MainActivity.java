/**  
 * MainActivity.java -Main entry point of the app.
 * @author  Shai Baz
 * @version 1.0 
 * @see Activity
 */

package com.beaconwatcher.demos;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;
import android.widget.Toast;

import com.radiusnetworks.ibeacon.BleNotAvailableException;
import com.radiusnetworks.ibeacon.IBeaconManager;

/**
 * This activity talks to the SDK by creating an instance of <b>iBeaconManager</b>
 * It checks if BLE is switched on, if not, it opens up settings screen to swtich Bluetooth ON.

 */
public class MainActivity extends Activity {
	private static final int REQUEST_ENABLE_BT = 999;
	private Context mContext;


	/**
	 * Used to get instance of IBeaconManager
	 */
	private IBeaconManager iBeaconManager;

	/**
	 * Boolean flag to check if Bluetooth enabled on this device.
	 */
	private Boolean mBtEnabled = false;

	/*
	 * UI elements.
	 */
	private TextView mText;



	/**
	 * Called when the activity is first created.
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		mContext=MainActivity.this;
		iBeaconManager = IBeaconManager.getInstanceForApplication(getApplicationContext());


		mText=(TextView) findViewById(R.id.txt_ble_not_available);

		findViewById(R.id.ble_button).setOnClickListener(mClickListener);
		findViewById(R.id.distance_demo_button).setOnClickListener(mClickListener);
		findViewById(R.id.notify_demo_button).setOnClickListener(mClickListener);
	}



	/**
	 * Check if Bluetooth is enabled on device.
	 */
	@Override
	protected void onResume() {
		super.onResume();
		try{
			//Check if Bluetooth LE is supported by this Android device,
			//and if so, make sure it is enabled.
			//Throws a BleNotAvailableException if Bluetooth LE is not supported.
			//(Note: The Android emulator will do this)

			//BLE is supported and Bluetooth is enabled.    	
			if (iBeaconManager.checkAvailability()) {
				bluetoothEnabled();
			}

			//BLE supported but Bluetooth is not enabled.
			else{
				bluetoothNotEnabled();
			}
		}

		//BLE is not supported
		catch(BleNotAvailableException e){
			findViewById(R.id.ble_button).setVisibility(View.GONE);
			mText.setText("Your device does not support BLE (Bluetooth 4.0), this demo will not work on your device. Please use a BLE supported device to test this Application.");
		}
	}


	private void bluetoothEnabled(){
		mBtEnabled=true;
		mText.setVisibility(View.GONE);
		findViewById(R.id.ble_button).setVisibility(View.GONE);
		findViewById(R.id.distance_demo_button).setVisibility(View.VISIBLE);
		findViewById(R.id.notify_demo_button).setVisibility(View.VISIBLE);
	}

	private void bluetoothNotEnabled(){
		mBtEnabled=false;
		mText.setVisibility(View.VISIBLE);
		findViewById(R.id.ble_button).setVisibility(View.VISIBLE);
		findViewById(R.id.distance_demo_button).setVisibility(View.GONE);
		findViewById(R.id.notify_demo_button).setVisibility(View.GONE);
	}





	//Global On click listener for all buttons
	final OnClickListener mClickListener = new OnClickListener() {
		public void onClick(final View v) {
			switch(v.getId()) {
			case R.id.distance_demo_button:
				if(mBtEnabled){
					Intent intent = new Intent(MainActivity.this, ListBeaconsActivity.class);
					intent.putExtra(ListBeaconsActivity.EXTRAS_TARGET_ACTIVITY, DistanceActivity.class.getName());
					startActivity(intent);
				}
				break;

			case R.id.notify_demo_button:
				if(mBtEnabled){
					Intent intent = new Intent(MainActivity.this, ListBeaconsActivity.class);
					intent.putExtra(ListBeaconsActivity.EXTRAS_TARGET_ACTIVITY, NotificationActivity.class.getName());
					startActivity(intent);
				}                
				break;


			case R.id.ble_button:
				Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
				startActivity(enableBtIntent);
				break;
			}
		}
	};
}
