/**  
 * DistanceBackgroundView.java -Displays distance to the beacon.
 * @author  Shai Baz
 * @version 1.0 
 * @see View
 */

package com.beaconwatcher.demos;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;

/**
 * Draws distance background that is stretched to parent's height, keeps aspect ration
 * and centers the image.
 */
public class DistanceBackgroundView extends View {

  private final Drawable mDrawable;

  public DistanceBackgroundView(Context context, AttributeSet attrs) {
    super(context, attrs);
    mDrawable = context.getResources().getDrawable(R.drawable.bg_distance);
  }

  @Override
  protected void onDraw(Canvas canvas) {
    super.onDraw(canvas);

    int width = mDrawable.getIntrinsicWidth() * canvas.getHeight() / mDrawable.getIntrinsicHeight();
    int deltaX = (width - canvas.getWidth()) / 2;
    mDrawable.setBounds(-deltaX, 0, width - deltaX, canvas.getHeight());
    mDrawable.draw(canvas);
  }
}
