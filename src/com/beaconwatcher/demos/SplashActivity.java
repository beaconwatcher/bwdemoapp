/**  
* SplashActivity.java-A splash screen that stays for 2 seconds on startup.
* @author  Shai Baz
* @version 1.0 
* @see Activity
*/

package com.beaconwatcher.demos;
 
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

/*
 * Showing splash screen with a timer. This will be useful when you
 * want to show case your app logo / company
 */
public class SplashActivity extends Activity {
 
    /*
     * Number of milliseconds for the splash screen to be shown. 
     */
    private static int SPLASH_TIME_OUT = 2000;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                // This method will be executed once the timer is over
                // Start your app main activity
                Intent i = new Intent(SplashActivity.this, MainActivity.class);
                startActivity(i);
                 // close this activity
                finish();
            }
        }, SPLASH_TIME_OUT);
    }
}