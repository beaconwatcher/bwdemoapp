/**  
 * ListBeaconsActivity.java -Lists all beacons around your android device.
 * @author Shai Baz
 * @version 1.0 
 * @see IBeaconManager
 * @see IBeaconConsumer
 * @see RangeNotifier
 */
package com.beaconwatcher.demos;


import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.RemoteException;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.beaconwatcher.demos.CustomAdapters.LeDeviceListAdapter;
import com.radiusnetworks.ibeacon.IBeacon;
import com.radiusnetworks.ibeacon.IBeaconConsumer;
import com.radiusnetworks.ibeacon.IBeaconDataNotifier;
import com.radiusnetworks.ibeacon.IBeaconManager;
import com.radiusnetworks.ibeacon.RangeNotifier;
import com.radiusnetworks.ibeacon.Region;
import com.radiusnetworks.ibeacon.client.DataProviderException;

/**
 * Displays list of found beacons sorted by RSSI.
 * Starts new activity with selected beacon if activity was provided.
 */
public class ListBeaconsActivity extends Activity implements IBeaconConsumer, RangeNotifier{
  private static final String TAG = ListBeaconsActivity.class.getSimpleName();
  public static final String EXTRAS_TARGET_ACTIVITY = "";
  
  private Context mContext;
  
  /**
   * Reference to main application controller
   */
  private ApplicationController mApp;
  
  /**
   * instance variable for interacting with SDK.
   */
  private IBeaconManager iBeaconManager;

  /**
   * Adapter for beacons list.
   */
  private LeDeviceListAdapter mAdapter;
  

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_list_beacons);
    
    mContext=ListBeaconsActivity.this;
    mApp = (ApplicationController) getApplicationContext();
    
    // Configure device list.
    mAdapter = new LeDeviceListAdapter(this);
    ListView list = (ListView) findViewById(R.id.device_list);
    list.setAdapter(mAdapter);
    list.setOnItemClickListener(createOnItemClickListener());
    
    // Configure BeaconManager.
    iBeaconManager = IBeaconManager.getInstanceForApplication(getApplicationContext());
  }
  
  
  
  /**
   * Called when the iBeacon service is running and ready to accept your commands through the IBeaconManager
   */
  @Override
  public void onIBeaconServiceConnect() {
	  //Define a Region by providing null for UUID, Major and Minor.
	  //setting null for all parameters create a Region that accepts 
	  //Any beacons regardless of uuid, major, minor.
	  Region region = new Region("MainActivityRanging", null, null, null);
      try {

    	  //Tell iBeaconManager to notify this Activity
    	  //upon finding any beacons in the Region.
          iBeaconManager.setRangeNotifier(this);
    	  
    	  
    	  //This will start Ranging feature, it will scan all BLE devices 
    	  //every 3 seconds and set the results via
    	  //callback method "didRangeBeaconsInRegion.
          iBeaconManager.startRangingBeaconsInRegion(region);
      } catch (RemoteException e) {
          e.printStackTrace();
      }
  }
  
  
  /**
   * Interface method invoked by IBeaconManager.
   * @param iBeacons Collection object of type IBeacon.
   * @param region Region object.
   */

  @Override
  public void didRangeBeaconsInRegion(final Collection<IBeacon> iBeacons, final Region region) {
	// Note that results are not delivered on UI thread.
      runOnUiThread(new Runnable() {
        @Override
        public void run() {
          // Note that beacons reported here are already sorted by estimated
          // distance between device and beacon.
          getActionBar().setSubtitle("Found beacons: " + iBeacons.size());
          //Update adapter.
          mAdapter.updateItems(iBeacons);
        }
     });
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    getMenuInflater().inflate(R.menu.scan_menu, menu);
    MenuItem refreshItem = menu.findItem(R.id.refresh);
    refreshItem.setActionView(R.layout.actionbar_indeterminate_progress);
    return true;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    if (item.getItemId() == android.R.id.home) {
      finish();
      return true;
    }
    return super.onOptionsItemSelected(item);
  }

  @Override
  protected void onStart() {
    super.onStart();
    
    //Set scan period to almost 3 seconds.
    //we set 3300 miliseconds just to fix a bug in Android Bluetooth.
    //If we see 3000 it ignores some scan results.
    iBeaconManager.setForegroundBetweenScanPeriod(3300);
    
    //Bind this activity to main Service running through IBeaconManager.
    iBeaconManager.bind(this);
  }

  @Override
  protected void onStop() {
	  super.onStop();
	  
	  iBeaconManager.setForegroundBetweenScanPeriod(0);
	  
	  //Unbind service.
	  if(iBeaconManager.isBound(this)){
          iBeaconManager.setRangeNotifier(this);
	  }
  }
  
  @Override
	protected void onPause() {
		super.onPause();
		
		iBeaconManager.setForegroundBetweenScanPeriod(0);
		
		  if(iBeaconManager.isBound(this)){
	          iBeaconManager.setRangeNotifier(this);
		  }
	}
  
  @Override
	protected void onResume() {
		super.onResume();
		
		iBeaconManager.setForegroundBetweenScanPeriod(3300);
		iBeaconManager.bind(this);
	}
  
  

  @Override
  public void onDestroy() {
      super.onDestroy();
      iBeaconManager.setForegroundBetweenScanPeriod(0);
      
	  if(iBeaconManager.isBound(this)){
	      iBeaconManager.unBind(this);
	  }
  }
  

  private AdapterView.OnItemClickListener createOnItemClickListener() {
    return new AdapterView.OnItemClickListener() {
      @Override
      public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
    	  mApp.setSelectedBeacon(mAdapter.getItem(position));
    	  
    	  //If there is an Activity reference passed to this activity's extras.
    	  if (getIntent().getStringExtra(EXTRAS_TARGET_ACTIVITY) != null) {
    		  try {
    			  Class<?> clazz = Class.forName(getIntent().getStringExtra(EXTRAS_TARGET_ACTIVITY));
    			  Intent intent = new Intent(ListBeaconsActivity.this, clazz);
    			  // 	intent.putExtra(ConnectionActivity.EXTRAS_DEVICE_ADDRESS, mAdapter.getItem(position).getMacAddress());
    			  
    			  //Set reference of selected beacon in Application Controller
    			  //to be used in any other activity.
    			  mApp.setSelectedBeacon(mAdapter.getItem(position));
    			  startActivity(intent);
    		  } catch (ClassNotFoundException e) {
    			  Log.e(TAG, "Finding class by name failed", e);
    		  }
    	  }
      }
    };
  }
}
