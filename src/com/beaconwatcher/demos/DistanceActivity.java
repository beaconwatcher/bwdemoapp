/**  
 * DistanceActivity.java -Displays distance to the beacon.
 * @author  Shai Baz
 * @version 1.0 
 * @see RangeNotifier
 * @see IBeaconConsumer
 * @see IBeaconDataNotifier
 */

package com.beaconwatcher.demos;

import java.text.DecimalFormat;
import java.util.Collection;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.RemoteException;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.TextView;
import android.widget.Toast;

import com.radiusnetworks.ibeacon.IBeacon;
import com.radiusnetworks.ibeacon.IBeaconConsumer;
import com.radiusnetworks.ibeacon.service.IBeaconData;
import com.radiusnetworks.ibeacon.IBeaconDataNotifier;
import com.radiusnetworks.ibeacon.IBeaconManager;
import com.radiusnetworks.ibeacon.RangeNotifier;
import com.radiusnetworks.ibeacon.Region;
import com.radiusnetworks.ibeacon.client.DataProviderException;

/**
 * Visualizes distance from beacon to the device.
 */
public class DistanceActivity extends Activity implements IBeaconConsumer, RangeNotifier, IBeaconDataNotifier  {

	private static final String TAG = DistanceActivity.class.getSimpleName();
	private Context mContext;
	private ApplicationController mApp;


	// Y positions are relative to height of bg_distance image.
	private static final double RELATIVE_START_POS = 320.0 / 1110.0;
	private static final double RELATIVE_STOP_POS = 885.0 / 1110.0;

	private IBeaconManager beaconManager;
	private IBeacon mBeacon;
	private Region mRegion;

	private View mDotView;
	private int mStartY = -1;
	private int mSegmentLength = -1;

	private TextView mDistanceText;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_distance);

		mContext=DistanceActivity.this;
		mApp = (ApplicationController) getApplicationContext();

		getActionBar().setDisplayHomeAsUpEnabled(true);

		//UI elements
		mDotView = findViewById(R.id.dot);
		mDistanceText = (TextView) findViewById(R.id.distance_text);

		/**
		 * instance variable for interacting with SDK.
		 */
		beaconManager = IBeaconManager.getInstanceForApplication(this.getApplicationContext());


		final View view = findViewById(R.id.sonar);
		view.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
			@Override
			public void onGlobalLayout() {
				view.getViewTreeObserver().removeOnGlobalLayoutListener(this);

				mStartY = (int) (RELATIVE_START_POS * view.getMeasuredHeight());
				int stopY = (int) (RELATIVE_STOP_POS * view.getMeasuredHeight());
				mSegmentLength = stopY - mStartY;

				mDotView.setVisibility(View.VISIBLE);
			}
		});
	}


	@Override
	public void onIBeaconServiceConnect() {
		//Get reference of selected beacon from last activity.
		mBeacon=mApp.getSelectedBeacon();
		
		if(mBeacon == null) {
			Toast.makeText(this, "Beacon not found in intent extras", Toast.LENGTH_LONG).show();
			finish();
		}
		
		try {
			beaconManager.setRangeNotifier(this);
			
			//define new region to monitor only selected beacon
			//by providing uuid, major and minor values to constructor.
			mRegion = new Region("regionid", mBeacon.getProximityUuid(), mBeacon.getMajor(), mBeacon.getMinor());
			
			//Start ranging selected beacon only.
			beaconManager.startRangingBeaconsInRegion(mRegion);

		} catch (RemoteException e) {
			e.printStackTrace();
		}
	}


	@Override
	public void iBeaconDataUpdate(IBeacon iBeacon, IBeaconData iBeaconData, DataProviderException e) {
		if (e != null) {
			Log.d(TAG, "data fetch error:"+e);
		}
		if (iBeaconData != null) {
			//   Log.d(TAG, "I have an iBeacon with data: uuid="+iBeacon.getProximityUuid()+" major="+iBeacon.getMajor()+" minor="+iBeacon.getMinor()+" welcomeMessage="+iBeaconData.get("welcomeMessage"));
			//  String displayString = iBeacon.getProximityUuid()+" "+iBeacon.getMajor()+" "+iBeacon.getMinor()+"\n"+"Welcome message:"+iBeaconData.get("welcomeMessage");
			updateDistanceView(iBeacon);//, displayString, true);
		}
	}



	private void updateDistanceView(IBeacon foundBeacon) {
		if (mSegmentLength == -1) {
			return;
		}

		mDotView.animate().translationY(computeDotPosY(foundBeacon)).start();
	}

	private int computeDotPosY(IBeacon beacon) {
		// Let's put dot at the end of the scale when it's further than 6m.
		double distance = Math.min(beacon.getAccuracy(), 40.0);
		mDistanceText.setText(new DecimalFormat("##.##").format(distance)+"m");
		return mStartY + (int) (mSegmentLength * (distance / 40.0));
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == android.R.id.home) {
			finish();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	protected void onStart() {
		super.onStart();
		// Check if device supports Bluetooth Low Energy.
		if (! beaconManager.checkAvailability()) {
			Toast.makeText(mContext, "Device does not have Bluetooth Low Energy", Toast.LENGTH_LONG).show();
			return;
		}
		beaconManager.bind(this);
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		beaconManager.unBind(this);
		super.onPause();
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		beaconManager.bind(this);
		super.onResume();
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		beaconManager.unBind(this);
		super.onDestroy();
	}

	@Override
	protected void onStop() {
		beaconManager.unBind(this);
		super.onStop();
	}

	@Override
	public void didRangeBeaconsInRegion(final Collection<IBeacon> iBeacons, final Region region) {
		// Note that results are not delivered on UI thread.
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				// Note that beacons reported here are already sorted by estimated
				if(iBeacons.size() > 0){
					//update distance dot on view.
					updateDistanceView((IBeacon) iBeacons.toArray()[0]);
				}
			}
		});
	}
}
